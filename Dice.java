/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Random;

public class Dice {
    Random rm ;
    int maxValue;
    Dice(){
       rm = new Random(System.nanoTime());
       maxValue = 6;
    }
    Dice(int maxValue){
        rm = new Random(System.nanoTime());
        this.maxValue =  maxValue;
    }
    int roll(){
        return rm.nextInt(maxValue) + 1;
    }            
}
