public class GameBoard {
	private int[] squares;
	private int size;

	public GameBoard() {
		this(100);
	}

	public GameBoard(int size) {
		this.size = size;
		squares = new int[size + 1];
		for (int i = 0; i <= size; ++i) {
			squares[i] = i;
		}
	}

	public int size() {
		return this.size;
	}

	int nextPosition(int position, int steps) {
		int nextPosition = position + steps;
		if (nextPosition > this.size) {
			return position;
		}
		return this.squares[nextPosition];
	}

	public int startingSquare() {
		return this.squares[0];
	}

	public int endingSquare() {
		return this.squares[this.size];
	}

	void addSnake(int head, int tail) {
		this.squares[head] = tail;
	}

	void addLadder(int top, int foot) {
		this.squares[foot] = top;
	}
}
