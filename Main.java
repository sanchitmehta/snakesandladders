public class Main {
	public static void main(String[] args) throws Exception {
		SnakeAndLadder snakeAndLadder;
		if(args.length == 0){
			snakeAndLadder = new SnakeAndLadder();
		} else if(args.length == 1){
			snakeAndLadder = new SnakeAndLadder(Integer.parseInt(args[0]));
		} else {
			snakeAndLadder = new SnakeAndLadder(Integer.parseInt(args[0]), args[1]);
		}
		snakeAndLadder.play();
		System.out.println(snakeAndLadder.getHistory());
	}
}