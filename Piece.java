public class Piece {

	private String color;
	private int position;
	private GameBoard gameBoard;

	Piece(String color) {
		this.color = color;
		this.position = 0;
	}

	public int getPosition() {
		return this.position;
	}

	void placeOnBoard(GameBoard board) {
		this.gameBoard = board;
		this.position = board.startingSquare();
	}
	
	public void move(int steps) {
		if (this.gameBoard != null) {
			this.position = this.gameBoard.nextPosition(this.position, steps);
		}
	}

	public String toString() {
		return color;
	}
}

