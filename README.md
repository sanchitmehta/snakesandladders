Snakes and Ladders
===================

Snakes and Ladders in PayPal bootcamp, refactored.

Problem Statement: To design the Snakes & Ladder game for one or more than one players using Object Oriented Design

A configuration file is set up which takes the input in the following order:

Boardsize 100 // Size of the Board
Pieces Blue,Green,Red // Order of Colours for pieces
Snakes 98,2;72,34;57,25;47,15 // Positions of head and tails of Snakes
Ladders 28,11;62,32;97,36;87,51 //	Positions of top and foot of Ladders

There are 4 classes 

Dice.java
GameBoard.java
Piece.java
SnakeAndLadder.java
