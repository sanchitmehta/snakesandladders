import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;

public class SnakeAndLadder {

	private GameBoard board;
	private Piece pieces[];
	private Piece winner;
	private Piece currentPiece;
	private int noOfPieces;
	private String configFilePath;
	private ArrayList<String> logs;

	private static String DEFAULT_CONFIG_FILE_NAME = "game.cfg";
	
	SnakeAndLadder() throws IOException{
		this(4, DEFAULT_CONFIG_FILE_NAME);
	}

	SnakeAndLadder(String configFilePath) throws IOException {
		this(4, configFilePath);
	}
	
	SnakeAndLadder(int noOfPieces) throws IOException {
		this(noOfPieces, DEFAULT_CONFIG_FILE_NAME);
	}

	SnakeAndLadder(int noOfPieces, String configFilePath) throws IOException {
		this.configFilePath = configFilePath;
		this.noOfPieces = noOfPieces;
		initializeBoard();
		initializePieces();
		logs = new ArrayList<String>();
	}

	private void initializeBoard() throws IOException {
		board = new GameBoard(boardSizeFromConfig());
		createSnakes();
		createLadders();
	}
	
	private void createSnakes() throws IOException{
		int [][] snakes = new int[20][2];
		snakes = snakesFromConfig();
		for (int[] eachSnake:snakes) {
			board.addSnake(eachSnake[0], eachSnake[1]);
		}
	}
	
	private void createLadders() throws IOException{
		int [][] ladders = new int[20][2];
		ladders = laddersFromConfig();
		for (int[] eachLadder:ladders) {
			board.addLadder(eachLadder[0], eachLadder[1]);
		}
	}

	private void initializePieces() throws IOException {
		pieces = new Piece[noOfPieces];
		String pieceColor[] = piecesFromConfig();
		for (int i = 0; i < noOfPieces; i++) {
			pieces[i] = new Piece(pieceColor[i]);
			pieces[i].placeOnBoard(board);
		}
		winner = null;
	}

	private boolean gameNotOver() {
		return winner == null;
	}

	private boolean isWinner(Piece piece) {
		return piece.getPosition() == board.size();
	}

	public Piece getWinner() {
		return winner;
	}

	public void play() {
        Dice dice = new Dice();
		int turn = 0;
		int roll, oldPosition;
		currentPiece = pieces[0];
		while (gameNotOver()) {
			roll = dice.roll();
			oldPosition = currentPiece.getPosition();
			currentPiece.move(roll);
			if (isWinner(currentPiece)) {
                winner = currentPiece;
			}
			logMove(roll, oldPosition, currentPiece);

			if (roll != 6 || oldPosition > currentPiece.getPosition()) {
				currentPiece = pieces[++turn % noOfPieces];
			}
		}
	}

	private void logMove(int roll, int oldPosition, Piece piece) {
		int currentPosition = piece.getPosition();
		String snakeOrLadder = "";
		if (currentPosition - oldPosition < 0) {
			snakeOrLadder = "S";
		} else if (currentPosition > (oldPosition + roll)) {
			snakeOrLadder = "L";
		}
		logs.add(String.format("%s\t%d\t%d\t%s\n", piece, roll, piece.getPosition(), snakeOrLadder));
	}

	public String getHistory() {
		String log = "";
		for (int i = 0; i < logs.size(); ++i) {
			log += logs.get(i);
		}
		return log;
	}

	public String getLastLog() {
		if (logs.size() > 0) {
			return logs.get(logs.size() - 1);
		}
		return "";
	}
	
	private int boardSizeFromConfig() throws NumberFormatException, IOException{
		return Integer.parseInt(configValue("Boardsize"));
		
	}
	
	public String[] piecesFromConfig() throws IOException{
		return configValue("Pieces").split(",");
	}
	
	public int[][] snakesFromConfig() throws IOException{
		int snakes[][] = new int[20][2];
		String s[] = configValue("Snakes").split(";");
		for (int i = 0; i < s.length; i++) {
			snakes[i][0] = Integer.parseInt(s[i].split(",")[0]);
			snakes[i][1] = Integer.parseInt(s[i].split(",")[1]);
		}
		return snakes;
		
	}
	
	public int[][] laddersFromConfig() throws IOException{
		int ladders[][] = new int[20][2];
		String s[] = configValue("Ladders").split(";");
		for (int i = 0; i < s.length; i++) {
			ladders[i][0] = Integer.parseInt(s[i].split(",")[0]);
			ladders[i][1] = Integer.parseInt(s[i].split(",")[1]);
		}
		return ladders;
	}
	
	public String configValue(String configName) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(configFilePath));
		String sCurrentLine = new String();
		while ((sCurrentLine = br.readLine()) != null) {
			String eachConfig[] = sCurrentLine.split(" ");
			if(eachConfig[0].equals(configName)) {
				return eachConfig[1];
			}
		}
		return "";
	}
}
